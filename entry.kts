#!/usr/bin/env kscript

@file:DependsOn("it.skrape:skrapeit-core:1.0.0-alpha6")

import it.skrape.core.htmlDocument
import it.skrape.extract
import it.skrape.skrape

val hackerNewsUrl = "https://news.ycombinator.com/"

scrapeAllLinks().println()

fun scrapeAllLinks() = skrape {
    url = hackerNewsUrl
    extract {
        htmlDocument { findAll("a.storylink").map {
            Pair(it.text, it.attribute("href"))
        } }
    }
}

fun List<Pair<String, String>>.println() {
    this.forEach {
        println("title: ${it.first}")
        println("href: ${it.second}")
        println("=====")
        println("")
    }
}